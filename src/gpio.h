
#include <stdlib.h>  
#include<stdbool.h>
 
void gpio_default(uint16_t * puerto);
void gpio_setAngle(int angle);
void gpio_read(void);
bool gpio_compareInOut(uint16_t * puerto);
void gpio_ManualPhase(void);