#include "unity.h" //---PASO 1
#include "gpio.h" // la función a evaluar 


void setUp(void) //----PASO 1
    {  // las cosas que son comunes en los test se pueden colocar aqui
        // como variables y funciones
        //primero se ejecuta este setUp antes de cada test 

    }
void tearDown(void) 
    {

    }
void test_gpioDefoultAngle(void)   /// PRIMER TEST , El requetimiento indica que el angulo por defecto 
                                    // es decir el de inicio de sistema sea el de 0xFFF7
    {                       
       uint16_t gpioValue = 0xFFFF;
  
        gpio_defoult(&gpioValue); 
       
        TEST_ASSERT_EQUAL_HEX16(0xFFF7,gpioValue); 
        

    } 
void test_setWarningAngle(void)  // SEGUNDO TEST, el requerimiento indica que en caso de perdida de enerfia
                                 // el angulo que debe cargarse es el 90º
    {  
        uint16_t gpioValue = 0xFFFF;
        int angle;
        load_memory();
        gpio_setAngle(angle); // Warning angle = 90º
        TEST_ASSERT_EQUAL_HEX16(0,gpioValue);
    }
void test_txRealPhase(void)    // TERCER TEST , el requerimiento indica que la phase calculada entre la deseada y la salida
                               // debe ser la misma que se envia para la transmision por ethernet
    {
        uint16_t phase= 0;  // variable que retornará con valor de fase calculada
        uint16_t txphase=0; // valor transmitido via ethernet

        readADC();
        phase=calculatePhase();
        txPhase(phase);
        TEST_ASSERT_EQUAL_HEX16(phase,txphase);
    }
void test_failCircuits(void)   // CUARTO TEST, el requetimiento indica que se deben detectar los errores de GPIOS seleccionados
                                // y GPIOS como inputs ya que la funcionalidad de tener GPIOS post Reles es para ello
    {
        uint16_t gpioValue = 0xFFFF;
        bool inEqualOut = true;

        int angle;

        gpio_setAngle(angle);
        gpio_read();
        gpio_compareInOut(gpioValue);

        EST_ASSERT_EQUAL_HEX16(false,inEqualOut);
    }
void test_withOutConexion(void)  // QUINTO TEST, el requetimiento indica que debe ser posible hacer cambios de fase de manera manual
                                // es decir deben existir switches para seleccionar la fase
    {   
        bool inEqualOut = true;
        uint16_t gpioValue = 0xFFFF;
        
        gpio_ManualPhase();
        gpio_compareInOut(gpioValue);
        EST_ASSERT_EQUAL_HEX16(true,inEqualOut);

    }
